<?php

return [

    'pdo' => [
        'driver' => 'mysql',
        'host' => 'CHANGEME',
        'db_name' => 'tibame',
        'db_username' => 'tibame',
        'db_user_password' => 'tibame',
        'default_fetch' => PDO::FETCH_OBJ,
    ],
    'mysqli' => [
        'host' => 'CHANGEME',
        'db_name' => 'tibame',
        'db_username' => 'tibame',
        'db_user_password' => 'tibame',
        'default_fetch' => MYSQLI_ASSOC,
    ],
];